#include <iostream>
#include <vector>

#include <rx/observable.hpp>
#include <rx/subject.hpp>

#include <rx/operator.hpp>
#include <rx/operators/distinct.hpp>
#include <rx/operators/map.hpp>
#include <rx/operators/filter.hpp>
#include <rx/operators/subscribe.hpp>

#include <rx/sources/of.hpp>
#include <rx/sources/range.hpp>

int main() {
    using namespace rx::sources;
    using namespace rx::operators;

    auto r = range(1, 20);

    r
        | filter([] (int i, int) { return i > 5; })
        | filter([] (int, int index) { return index % 2 == 1; })
        | map([] (int val) { return val + 0.15f; })
        | subscribe([] (float v) { std::cout << "Kappa! " << v << std::endl; });

    std::cout << "done" << std::endl;

    /* -------------------- */
    of(std::vector{1, 1, 2, 3, 3, 2, 1, 2, 2})
        | distinct_until_changed()
        | subscribe([] (int v) { std::cout << "Plouf: " << v << std::endl; });


    /*
    subject<int> store{};
    auto state = store.as_observable();



    while (auto c = std::getchar()) {
        if (c == 'q')
            break;

    }
     */

    return 0;
}
