#pragma once

#include <rx/observable.hpp>

namespace rx::operators {
    template<class T>
    struct subscribe {
        rx::subscriber <T> sub;

        subscribe(next_t<T> && next)
                : sub(std::move(next)) {}

        subscribe(next_t<T> && next, complete_fn && complete)
            : sub(std::move(next), std::move(complete)) {}

        template<class Obs>
        auto operator()(Obs &&src) const {
            std::forward<Obs>(src).subscribe(sub);
        }
    };

    template < class Closure >
    subscribe(Closure) -> subscribe<typename utils::func_decomp<Closure>::first_type>;

    template < class Closure, class CompleteFn >
    subscribe(Closure, CompleteFn) -> subscribe<typename utils::func_decomp<Closure>::first_type>;
}
