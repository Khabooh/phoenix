#pragma once

#include <rx/observable.hpp>

namespace rx::operators::details {
    template<class T>
    struct filter {
        using filter_fn = std::function<bool(T, int)>;

        filter_fn test;

        filter(filter_fn fn) : test(fn) {}

        template<class Subscriber>
        struct observer {
            Subscriber dest;
            filter_fn test;
            mutable int index;

            observer(Subscriber sub, filter_fn test) : dest(sub), test(test), index(0) {}

            template<class Value>
            auto next(Value &&val) const {
                if (test(val, index++)) {
                    dest.next(std::forward<Value>(val));
                }
            }

            void complete() const {
                dest.complete();
            }
        };

        template<class Subscriber>
        auto operator()(Subscriber sub) const {
            return observer(sub, test);
        }
    };
}

namespace rx::operators {
    template<class Func>
    auto filter(Func &&test) {
        using T = typename utils::func_decomp<Func>::first_type;
        return [&](auto source) {
            return source.template lift<T>(details::filter<T>(std::forward<Func>(test)));
        };
    }
}
