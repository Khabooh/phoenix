#pragma once

#include <rx/observable.hpp>

namespace rx::operators::details {
    template<class T>
    struct distinct_until_changed {
        distinct_until_changed() = default;

        template < class Subscriber >
        struct observer {
            Subscriber dest;
            mutable T save;

            observer(Subscriber sub) : dest(sub) {}

            template<class Value>
            auto next(Value &&val) const {
                if (val != save) {
                    save = val;
                    dest.next(std::forward<Value>(val));
                }
            }

            void complete() const {
                dest.complete();
            }
        };

        template<class Subscriber>
        auto operator()(Subscriber sub) const {
            return observer(sub);
        }
    };
}

namespace rx::operators {
    auto distinct_until_changed() {
        return [&] < class T, class Op > (observable<T, Op> source) {
            return source.template lift<T>(details::distinct_until_changed<T>());
        };
    }
}
