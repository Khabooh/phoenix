#pragma once

#include <rx/observable.hpp>

namespace rx::operators::details {
    template<class In, class Out>
    struct map {
        using map_fn = std::function<Out(In)>;

        map_fn func;

        map(map_fn && f) : func(std::move(f)) {}

        template<class Subscriber>
        struct observer {
            Subscriber dest;
            map_fn func;

            observer(Subscriber sub, map_fn func) : dest(std::move(sub)), func(func) {}

            template<class Value>
            auto next(Value &&val) const {
                dest.next(func(std::forward<Value>(val)));
            }

            void complete() const {
                dest.complete();
            }
        };

        template < class Subscriber >
        auto operator()(Subscriber sub) const {
            return observer(std::move(sub), func);
        }
    };
}

namespace rx::operators {
    template<class Func>
    auto map(Func &&func) {
        using In = typename utils::func_decomp<Func>::first_type;
        using Out = typename utils::func_decomp<Func>::ret_type;
        return [&](auto source) {
            return source.template lift<Out>(details::map<In, Out>(std::forward<Func>(func)));
        };
    }
}