#pragma once

#include <rx/observable.hpp>

namespace rx::sources::details {
    template<class T>
    struct of {
        T value;

        of(T && val) : value(std::move(val)) {}

        template<class Subscriber>
        auto on_subscribe(Subscriber && subscriber) const {
            for (auto const& i: value)
                subscriber.next(i);
            subscriber.complete();
        }
    };
}

namespace rx::sources {
    template < template < class ... > class C, class T >
    auto of(C<T> value) {
        using container_t = C<T>;
        return observable<T, details::of<container_t>>(details::of<container_t>(std::move(value)));
    }
}