#pragma once

#include <rx/observable.hpp>

namespace rx::sources::details {
    template<class T>
    struct range {
        T first, last, step;

        range(T first, T last, T step) : first(first), last(last), step(step) {}

        template<class Subscriber>
        auto on_subscribe(Subscriber && subscriber) const {
            for (auto i = first; i < last; i += step)
                subscriber.next(i);
            subscriber.complete();
        }
    };
}

namespace rx::sources {
    template<class T>
    auto range(T first, T last, T step = 1) {
        return observable<T, details::range<T>>(details::range<T>(first, last, step));
    }
}