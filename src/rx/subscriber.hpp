#pragma once

#include <cassert>
#include <rx/utils.hpp>

namespace rx {
    template<class T>
    using next_t = std::function<void(T)>;
    using complete_fn = std::function<void()>;

    template<class T>
    struct subscriber {
        using next_fn = next_t<T>;

        next_fn next = [](T) {
            std::cout << "A next function was lost next stuff...\n";
            assert(false);
        };
        complete_fn complete = [] {};

        subscriber() = default;
        subscriber(next_fn n) : next(std::move(n)) {}
        subscriber(next_fn n, complete_fn f) : next(n), complete(std::move(f)) {}
    };
}