#pragma once

#include <functional>

namespace rx::utils {
    template < class, class ... >
    struct func_decomp;

    template < class Ret, class Class, class First, class ... Args >
    struct func_decomp<Ret(Class::*)(First, Args...)> {
        using first_type = First;
        using ret_type = Ret;
    };

    template < class Ret, class Class, class First, class ... Args >
    struct func_decomp<Ret(Class::*)(First, Args...) const> {
        using first_type = First;
        using ret_type = Ret;
    };

    template < class Func >
    struct func_decomp<Func> {
        using resolved_t = func_decomp<decltype(&Func::operator())>;
        using first_type = typename resolved_t::first_type;
        using ret_type = typename resolved_t::ret_type;
    };


}