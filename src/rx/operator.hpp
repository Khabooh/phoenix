#pragma once

#include <rx/subscriber.hpp>

namespace rx {
    template<class, class SourceOperator, class Operator>
    struct lift_operator {
        SourceOperator src;
        Operator chain;

        lift_operator(SourceOperator source, Operator && op)
            : src(std::move(source)),
            chain(std::move(op)) {}

        template<class Subscriber>
        auto on_subscribe(Subscriber sub) const {
            auto lifted = chain(std::move(sub));
            src.on_subscribe(std::move(lifted));
        }
    };
}