#pragma once

#include <rx/operator.hpp>

namespace rx {
    template<class T, class source_operator_t>
    struct observable {
        source_operator_t source_operator;

        observable() = default;
        observable(source_operator_t && src_op) : source_operator(std::move(src_op)) {}

        template<class Op>
        auto op(Op && op) const {
            return std::forward<Op>(op)(*this);
        }

        template<class ... Args>
        void subscribe(Args &&... args) const {
            detail_subscribe(subscriber(std::forward<Args>(args)...));
        }

        template<class U >
        void subscribe(subscriber<U> && sub_next) const {
            detail_subscribe(std::move(sub_next));
        }

        template<class ResultType, class Operator>
        auto lift(Operator && op) const {
            return observable<ResultType, lift_operator<ResultType, source_operator_t, Operator>>
                    (lift_operator<ResultType, source_operator_t, Operator>(source_operator,
                                                                            std::forward<Operator>(op)));
        }

    private:
        template<class Subscriber>
        auto detail_subscribe(Subscriber && sub) const {
            source_operator.on_subscribe(std::forward<Subscriber>(sub));
        }
    };

    template<class T, class source_op, class Op>
    auto operator|(observable<T, source_op> const& obs, Op && op) {
        return obs.op(std::forward<Op>(op));
    }
}